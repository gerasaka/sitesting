import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

import { environment } from 'src/environments/environment';

import { Passenger } from '../interfaces/passenger.interface';

@Injectable({
  providedIn: 'root',
})
export class PassengerService {
  passengers: Subject<Passenger[]> = new BehaviorSubject<Passenger[]>([
    {
      id: 1,
      name: 'nama',
      city: 'kota',
    },
  ]);

  constructor(private http: HttpClient) {}

  getPassangers() {
    return this.http.get<Passenger[]>(environment.passangerApiUrl).subscribe({
      next: (res) => this.passengers.next(res),
      error: (err) => console.log(err, '<-- error when GET passangers'),
    });
  }

  addPassanger(newPassanger: Passenger) {
    return this.http
      .post<Passenger>(environment.passangerApiUrl, newPassanger)
      .subscribe({
        next: () => this.getPassangers(),
        error: (err) => console.log(err, '<-- error when POST passanger'),
      });
  }

  editPassenger(editedPassanger: Passenger) {
    return this.http
      .put<Passenger>(
        `${environment.passangerApiUrl}/${editedPassanger.id}`,
        editedPassanger
      )
      .subscribe({
        next: () => this.getPassangers(),
        error: (err) => console.log(err, '<-- error when PUT passanger'),
      });
  }

  deletePassenger(passangerId: number) {
    return this.http
      .delete(`${environment.passangerApiUrl}/${passangerId}`)
      .subscribe({
        next: () => this.getPassangers(),
        error: (err) => console.log(err, '<-- error when DELETE passanger'),
      });
  }
}
