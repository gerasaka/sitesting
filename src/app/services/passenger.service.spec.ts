import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

import { PassengerService } from './passenger.service';

import { environment } from 'src/environments/environment';

const MOCKED_PASSENGERS = [
  {
    id: 1,
    name: 'test',
    city: 'service',
  },
  {
    id: 2,
    name: 'karma',
    city: 'jasmine',
  },
];

const PASSENGER = {
  id: 3,
  name: 'budi',
  city: 'jakarta',
};

describe('PassengerService', () => {
  let service: PassengerService;

  let http: HttpClient;
  let controller: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(PassengerService);

    http = TestBed.inject(HttpClient);
    controller = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    controller.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return passengers when `getPassengers` called', () => {
    service.getPassangers();

    controller.expectOne(environment.passangerApiUrl).flush(MOCKED_PASSENGERS);

    service.passengers.subscribe({
      next: (res) => {
        expect(res).toEqual(MOCKED_PASSENGERS);
      },
    });
  });

  it('should add a passanger when `addPassenger` called', () => {
    service.addPassanger(PASSENGER);

    controller
      .expectOne({
        method: 'POST',
        url: environment.passangerApiUrl,
      })
      .flush(PASSENGER);

    controller
      .expectOne(environment.passangerApiUrl)
      .flush([...MOCKED_PASSENGERS, PASSENGER]);

    service.passengers.subscribe({
      next: (res) => {
        expect(res).toEqual([...MOCKED_PASSENGERS, PASSENGER]);
      },
    });
  });

  it('should edit passenger data when `editPassenger` called', () => {
    service.editPassenger(MOCKED_PASSENGERS[0]);

    controller
      .expectOne({
        method: 'PUT',
        url: `${environment.passangerApiUrl}/${1}`,
      })
      .flush({ ...PASSENGER, id: 1 });

    controller
      .expectOne(environment.passangerApiUrl)
      .flush([{ ...PASSENGER, id: 1 }, MOCKED_PASSENGERS[1]]);

    service.passengers.subscribe({
      next: (res) => {
        expect(res).toEqual([{ ...PASSENGER, id: 1 }, MOCKED_PASSENGERS[1]]);
      },
    });
  });

  it('should delete a passenger when `deletePassenger` called', () => {
    service.deletePassenger(MOCKED_PASSENGERS[0].id);

    controller
      .expectOne({
        method: 'DELETE',
        url: `${environment.passangerApiUrl}/${1}`,
      })
      .flush(1);

    controller
      .expectOne(environment.passangerApiUrl)
      .flush([MOCKED_PASSENGERS[1]]);

    service.passengers.subscribe({
      next: (res) => {
        expect(res).toEqual([MOCKED_PASSENGERS[1]]);
      },
    });
  });
});
