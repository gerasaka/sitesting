import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { LoginType } from '../interfaces/login.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  login(credentials: LoginType) {
    return this.http
      .post<any>('http://localhost:3001/user/login', credentials)
      .pipe(map((res) => res));
  }
}
