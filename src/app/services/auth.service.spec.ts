import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

import { AuthService } from './auth.service';
import { getMaxListeners } from 'process';

const USER = {
  username: 'budi@mail.com',
  password: '123456',
};

const MOCKED_RESPONSE = {
  status: 200,
  message: 'success',
  token: 'abc',
  role: 'member',
  user: {
    _id: 1,
    name: 'Budi anduk',
    email: 'budi@mail.com',
    role: 'member',
  },
};

let response_message: string;

describe('AuthService', () => {
  let service: AuthService;

  let http: HttpClient;
  let controller: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(AuthService);

    http = TestBed.inject(HttpClient);
    controller = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should response user credentials with `success`', () => {
    service.login(USER).subscribe({
      next: (res) => {
        response_message = res.message;
      },
    });

    controller
      .expectOne({
        method: 'POST',
        url: 'http://localhost:3001/user/login',
      })
      .flush(MOCKED_RESPONSE);

    expect(response_message).toEqual(MOCKED_RESPONSE.message);
  });
});
