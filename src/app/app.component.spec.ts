import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { PassengerService } from './services/passenger.service';

import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;

  let http: HttpClient;
  let service: PassengerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
    fixture.detectChanges();

    http = TestBed.inject(HttpClient);
    service = TestBed.inject(PassengerService);
  });

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it('should invoke `passengerService` when created', () => {
    const getPassengersSpy = spyOn(service, 'getPassangers');

    app.ngOnInit();

    expect(getPassengersSpy).toHaveBeenCalled();
  });
});
