import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IsLoginGuard } from './guards/is-login.guard';
import { LoginComponent } from './pages/login/login.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

import { PassengerComponent } from './pages/passenger/passenger.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [IsLoginGuard],
  },
  {
    path: 'passengers',
    component: PassengerComponent,
    canActivate: [IsLoginGuard],
  },
  {
    path: '',
    redirectTo: '/passengers',
    pathMatch: 'full',
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
