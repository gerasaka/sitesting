import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class IsLoginGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const currentPath: string = state.url.split('/')[1];
    const getToken = localStorage.getItem('access_token') ?? '';

    if (currentPath === 'login' && getToken?.length > 0) {
      this.router.navigate(['/passengers']);
    }

    if (currentPath === 'passengers' && getToken?.length === 0) {
      this.router.navigate(['/login']);
    }

    return true;
  }
}
