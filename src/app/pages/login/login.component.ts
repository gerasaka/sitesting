import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginType } from 'src/app/interfaces/login.interface';

import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [],
})
export class LoginComponent {
  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  constructor(private authService: AuthService) {}

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  onSubmit(credentials: LoginType | any) {
    if (credentials) {
      this.authService.login(credentials).subscribe((res) => {
        if (res.message === 'success' && res.token) {
          localStorage.setItem('access_token', res.token);
        }

        if (res.message === 'Not found') {
          alert('Anda belum terdaftar');
        }
      });
    }
  }
}
