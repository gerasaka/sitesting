import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';

import { AuthService } from 'src/app/services/auth.service';

import { LoginComponent } from './login.component';

const MOCKED_USER = {
  username: 'budi@mail.com',
  password: '123456',
};

const SUCCESS_RESPONSE = {
  status: 200,
  message: 'success',
  token: 'abc',
  role: 'member',
  user: {
    _id: 1,
    name: 'Budi anduk',
    email: 'budi@mail.com',
    role: 'member',
  },
};

const FAILED_RESONSE = {
  message: 'Not found',
};

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  let http: HttpClient;
  let controller: HttpTestingController;
  let service: AuthService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [HttpClientTestingModule, ReactiveFormsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    http = TestBed.inject(HttpClient);
    controller = TestBed.inject(HttpTestingController);
    service = TestBed.inject(AuthService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should authenticate user when `onSubmit` invoke', () => {
    component.onSubmit(MOCKED_USER);

    controller
      .expectOne({
        method: 'POST',
        url: 'http://localhost:3001/user/login',
      })
      .flush(MOCKED_USER);

    expect(localStorage.getItem('access_token')?.length).not.toBeNull();
  });

  it('alert user when authentication failed', () => {
    const alertSpy = spyOn(window, 'alert');

    spyOn(service, 'login').and.returnValue(of(FAILED_RESONSE));
    component.onSubmit(MOCKED_USER);

    expect(alertSpy).toHaveBeenCalledWith('Anda belum terdaftar');
  });

  it('should set `localstorage` when authentication success', () => {
    spyOn(service, 'login').and.returnValue(of(SUCCESS_RESPONSE));

    const storageSpy = spyOn(localStorage, 'setItem');

    component.onSubmit(MOCKED_USER);

    expect(storageSpy).toHaveBeenCalled();
  });
});
