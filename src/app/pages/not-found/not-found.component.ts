import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `
    <p>404</p>
    <p>Page Not Found</p>
  `,
  styles: [],
})
export class NotFoundComponent {}
