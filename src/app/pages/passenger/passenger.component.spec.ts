import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { PassengerService } from 'src/app/services/passenger.service';

import { PassengerComponent } from './passenger.component';

const PASSENGER_FORM_DATA = {
  name: 'test',
  city: 'component',
};

describe('PassengerComponent', () => {
  let component: PassengerComponent;
  let fixture: ComponentFixture<PassengerComponent>;

  let http: HttpClient;
  let service: PassengerService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PassengerComponent],
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(PassengerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    http = TestBed.inject(HttpClient);
    service = TestBed.inject(PassengerService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add passanger when invoke `onSubmit` and `isEdit` is false', () => {
    const asd = spyOn(service, 'addPassanger');

    component.onSubmit({ name: 'asdf', city: 'asdf' });

    expect(asd).toHaveBeenCalled();
  });

  it('should edit passanger when invoke `onSubmit` and `isEdit` is true', () => {
    const editSpy = spyOn(service, 'editPassenger');

    component.isEdit = true;
    component.onSubmit(PASSENGER_FORM_DATA);

    expect(editSpy).toHaveBeenCalled();
  });

  it('should add user when `isEdit` false', () => {
    component.onSubmit({ name: 'asdf', city: 'asdf' });

    expect(component.passengerForm.value.city).toEqual(null);
    expect(component.passengerForm.value.name).toEqual(null);
  });

  it('should delete passenger when `onDelete` invoke', () => {
    const deleteSpy = spyOn(service, 'deletePassenger');

    component.onDelete(1);

    expect(deleteSpy).toHaveBeenCalled();
  });

  it('should fill form field and set `isEdit` when `onEdit` invoke', () => {
    component.onEdit(0);

    expect(component.isEdit).toBeTrue();
    expect(component.passengerForm.value.name).not.toBeNull();
    expect(component.passengerForm.value.city).not.toBeNull();
  });
});
