import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { PassengerService } from 'src/app/services/passenger.service';

import { Passenger } from 'src/app/interfaces/passenger.interface';

@Component({
  selector: 'app-passenger',
  templateUrl: './passenger.component.html',
  styles: [],
})
export class PassengerComponent {
  isEdit: boolean = false;

  passengerForm = new FormGroup({
    id: new FormControl(`${Number(new Date())}`),
    name: new FormControl(''),
    city: new FormControl(''),
  });

  constructor(public passengerService: PassengerService) {}

  onSubmit({ name = 'default-name', city = 'default-city' }: any) {
    const passengerData: Passenger = {
      id: Number(this.passengerForm.value.id),
      name: name,
      city: city,
    };

    if (this.isEdit) {
      this.passengerService.editPassenger(passengerData);

      this.isEdit = false;
    } else {
      this.passengerService.addPassanger(passengerData);
    }

    this.passengerForm.reset({
      id: `${Number(new Date())}`,
    });
  }

  onDelete(id: number) {
    this.passengerService.deletePassenger(id);
  }

  onEdit(idx: any) {
    this.isEdit = true;

    this.passengerService.passengers
      .subscribe((val) => {
        this.passengerForm.setValue({
          id: `${val[idx].id}`,
          name: val[idx].name,
          city: val[idx].city,
        });
      })
      .unsubscribe();
  }
}
