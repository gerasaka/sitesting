import { Component, OnInit } from '@angular/core';
import { PassengerService } from './services/passenger.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [],
})
export class AppComponent implements OnInit {
  constructor(private passengerService: PassengerService) {}

  ngOnInit(): void {
    this.passengerService.getPassangers();
  }
}
