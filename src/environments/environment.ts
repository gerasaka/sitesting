const API_URL = 'https://6393019e11ed187986a7596b.mockapi.io/';

export const environment = {
  passangerApiUrl: `${API_URL}passangers`,
};
